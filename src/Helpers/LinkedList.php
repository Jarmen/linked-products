<?php

namespace lenal\linkedproducts\Helpers;

use SplDoublyLinkedList;

class LinkedList {

    protected $doubly_linked_list;

    public function __construct()
    {
        $this->doubly_linked_list = new SplDoublyLinkedList();
    }

    public function createLinkedList($products)
    {
        $i = 0;

        foreach ($products as $product) {
            $this->doubly_linked_list->add($i, $product->product_id);
            $i++;
        }

        return $this->doubly_linked_list;
    }

    public function calculateLinksCount($products_count)
    {
        $max_products = config('linkedproducts.ruleset.max_products_count');
        $max_links_number = config('linkedproducts.ruleset.max_links_count');

        if ($products_count >= $max_products) {
            return $max_links_number;
        }

        $ruleset = config('linkedproducts.ruleset.linked');

        if (! empty($ruleset[$products_count])) {
            return $ruleset[$products_count];
        }
    }

    public function createLinkedProducts($links_count, $skip_this)
    {
        $products_list = [];

        $list = $this->doubly_linked_list;

        $last_in_list = $list->top();

        for($list->rewind(); $list->valid(); $list->next()) {

            if ($list->current() <= $skip_this) {
                $list->next();
            }

            if ($links_count == 0) {
                break;
            }

            if ($list->current() == $last_in_list) {
                $products_list[] =  $list->current();
                $list->rewind();
            }

            $products_list[] =  $list->current();
            $links_count--;
        }

        return $products_list;
    }
}
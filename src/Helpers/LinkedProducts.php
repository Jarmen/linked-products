<?php

namespace lenal\linkedproducts\Helpers;

use lenal\linkedproducts\Models\LinkedOutOfProd;
use lenal\linkedproducts\Models\LinkedProduct;

class LinkedProducts {

    protected $linked_product;
    protected $out_of_production;

    public function __construct(LinkedProduct $linked_product, LinkedOutOfProd $linked_out_of_prod)
    {
        $this->linked_product = $linked_product;
        $this->out_of_production = $linked_out_of_prod;
    }

    public function getLinkedProducts($params)
    {
        return $this->linked_product->linkedProducts($params);
    }

    public function getLinkedOutOfProd($product_id)
    {
        return $this->out_of_production->linkedProducts($product_id);
    }
}
<?php

namespace lenal\linkedproducts\Controllers;

use App\Http\Controllers\Controller;
use emaz\productso\Models\PrsoProduct as Product;
use lenal\linkedproducts\Models\LinkedOutOfProd;

class LinkedOutOfProdParserController extends Controller
{
    protected $linked_product_model;

    public function __construct(LinkedOutOfProd $linked_out_of_prod)
    {
        $this->linked_product_model = $linked_out_of_prod;
    }

    public function parse()
    {
        $out_of_production = Product::leftJoin('prso_category_prso_product', 'prso_category_prso_product.prso_product_id', '=', 'prso_products.id')
            ->leftJoin('prso_categories', 'prso_categories.id', '=', 'prso_category_prso_product.prso_category_id')
            ->leftJoin('prso_product_prso_dimensions', 'prso_product_prso_dimensions.product_id', '=', 'prso_products.id')
            ->leftJoin('prso_options_value_prso_product', 'prso_options_value_prso_product.prso_product_id', '=', 'prso_products.id')
            ->leftJoin('prso_options_values', 'prso_options_values.id', '=', 'prso_options_value_prso_product.prso_options_value_id')
            ->where('prso_products.show', '=', 1)
            ->where('prso_products.status', '=', 4)
            ->where('prso_categories.depth', '=', 2)
            ->where('prso_options_values.option_id', '=', 1)
            ->get(
                [
                    'prso_products.id as product_id',
                    'prso_categories.id as category_id',
                    'prso_product_prso_dimensions.height',
                    'prso_products.cost',
                    'prso_options_values.value as color'
                ]
            );

        foreach ($out_of_production as $product) {
            $linked = $this->getTwelveLinkedProducts($product);

            if (! empty($linked)) {
                $this->saveData($product->product_id, $linked);
            }
        }
    }

    protected function getTwelveLinkedProducts($out_of_production)
    {
        $linked_diff = 0;

         $linked_products = Product::leftJoin('prso_category_prso_product', 'prso_category_prso_product.prso_product_id', '=', 'prso_products.id')
             ->leftJoin('prso_categories', 'prso_categories.id', '=', 'prso_category_prso_product.prso_category_id')
             ->leftJoin('prso_product_prso_dimensions', 'prso_product_prso_dimensions.product_id', '=', 'prso_products.id')
             ->where('prso_products.show', '=', 1)
             ->whereIn('prso_products.status', [1, 2])
             ->where('prso_categories.id', '=', $out_of_production->category_id)
             ->where(function ($query) use ($out_of_production) {
                 $this->defineQueryByHeight($out_of_production->height, $query);
             })
             ->where('prso_products.id', '!=', $out_of_production->product_id)
             ->limit(10)
             ->get();

         if ($linked_products->count() == 10) {
             return $linked_products;
         }

         if ($linked_products->count() < 10) {
             $linked_diff = 10 - $linked_products->count();
             $linked_auxiliary = $this->getAdditionalProducts(
                 $linked_diff,
                 $linked_products->pluck('product_id'),
                 $out_of_production
             );

             if (! empty($linked_auxiliary)) {
                 $merged = $linked_products->merge($linked_auxiliary);
                 $merged->all();
             }
         }

         return $merged;
    }

    protected function getAdditionalProducts($number_of_required, $excluded_products, $out_of_production)
    {
        $linked = Product::leftJoin('prso_category_prso_product', 'prso_category_prso_product.prso_product_id', '=', 'prso_products.id')
            ->leftJoin('prso_categories', 'prso_categories.id', '=', 'prso_category_prso_product.prso_category_id')
            ->leftJoin('prso_product_prso_dimensions', 'prso_product_prso_dimensions.product_id', '=', 'prso_products.id')
            ->leftJoin('prso_options_value_prso_product', 'prso_options_value_prso_product.prso_product_id', '=', 'prso_products.id')
            ->leftJoin('prso_options_values', 'prso_options_values.id', '=', 'prso_options_value_prso_product.prso_options_value_id')
            ->where('prso_products.show', '=', 1)
            ->whereIn('prso_products.status', [1, 2])
            ->where('prso_categories.id', '=', $out_of_production->category_id)
            ->whereNoTIn('prso_products.id', $excluded_products)
            ->where('prso_options_values.option_id', '=', 1)
            ->where('prso_options_values.value', '=', $out_of_production->color)
            ->limit($number_of_required)
            ->get(['prso_products.id as product_id']);

        return $linked;
    }

    protected function saveData($main_product_id, $linked)
    {
        foreach ($linked as $product) {
            if (empty($product->id)) {
                continue;
            }

            $this->linked_product_model->updateOrCreate([
                'out_of_prod_id' => $main_product_id,
                'linked_product_id' => $product->id
            ]);
        }
    }

    protected function defineQueryByHeight($height, $query)
    {
        if ($height <=  117) {
            $query->where('prso_product_prso_dimensions.height', '<=', 117);
        }

        if ($height >=  188) {
            $query->where('prso_product_prso_dimensions.height', '>=', 188);
        }

        if ($height > 117 && $height < 166) {
            $query->where('prso_product_prso_dimensions.height', '>=', 117)
                ->where('prso_product_prso_dimensions.height', '<=', 166);
        }

        if ($height >= 167 && $height <= 187 ) {
            $query->where('prso_product_prso_dimensions.height', '>=', 167)
                ->where('prso_product_prso_dimensions.height', '<=', 187);
        }

        return $query;
    }
}
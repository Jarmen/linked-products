<?php

namespace lenal\linkedproducts\Controllers;

use App\Http\Controllers\Controller;
use emaz\productso\Models\PrsoCategory as Category;
use emaz\productso\Models\PrsoOptionsValue as Value;
use lenal\linkedproducts\Models\LinkedProduct;

class LinkedProductsParserController extends Controller
{
    protected $install_method;
    protected $linked_product;

    public function __construct(LinkedProduct $linked_product)
    {
        $this->install_method = [
            '47', // отдельностоящий
            '982', // встраиваемый
            '1177' // под столешницу
        ];

        $this->linked_product = $linked_product;
    }
    public function parse()
    {
        $categories = Category::where('show', '=', 1)->pluck('id');

        $data = Value::leftJoin('prso_options_value_prso_product', 'prso_options_value_prso_product.prso_options_value_id', '=', 'prso_options_values.id')
            ->leftJoin('prso_category_prso_product', 'prso_category_prso_product.prso_product_id', '=', 'prso_options_value_prso_product.prso_product_id')
            ->leftJoin('prso_products', 'prso_products.id', '=', 'prso_options_value_prso_product.prso_product_id')
            ->whereIn('prso_category_prso_product.prso_category_id', $categories)
            ->whereIn('prso_options_value_prso_product.prso_options_value_id', $this->install_method)
            ->where('prso_products.show', '=', 1)
            ->where('prso_products.status', '!=', 4) // не снят с производства
            ->get([
                'prso_options_value_prso_product.prso_options_value_id',
                'prso_category_prso_product.prso_category_id',
                'prso_products.id',
                'prso_products.status'
            ]);

        $this->saveData($data);
    }

    protected function saveData($data)
    {
        foreach ($data as $product) {
            $this->linked_product->updateOrCreate([
                'category_id' => $product->prso_category_id,
                'product_id' => $product->id,
                'status' => $product->status,
                'install_method' => $product->prso_options_value_id
            ]);
        }
    }
}
<?php

namespace lenal\linkedproducts\Facades;

use Illuminate\Support\Facades\Facade;

class LinkedProducts extends Facade {

    public static function getFacadeAccessor()
    {
        return 'linkedproducts';
    }
}
<?php

namespace lenal\linkedproducts\Facades;

use Illuminate\Support\Facades\Facade;

class LinkedList extends Facade {

    public static function getFacadeAccessor()
    {
        return 'linkedlist';
    }
}
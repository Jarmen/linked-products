<?php

namespace lenal\linkedproducts\Models;

use Illuminate\Database\Eloquent\Model;

class LinkedOutOfProd extends Model
{
    public $timestamps = false;

    protected $table = 'linked_out_of_prod';
    protected $fillable = [
        'out_of_prod_id', 'linked_product_id'
    ];

    public function scopeLinkedProducts($query, $product_id)
    {
        return $query->where('out_of_prod_id', '=', $product_id)
            ->orderBy('linked_product_id');
    }
}

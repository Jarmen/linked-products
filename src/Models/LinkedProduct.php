<?php

namespace lenal\linkedproducts\Models;

use Illuminate\Database\Eloquent\Model;

class LinkedProduct extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'category_id', 'product_id', 'status', 'install_method'
    ];

    public function scopeLinkedProducts($query, $params)
    {
        return $query->where('category_id', '=', $params['category_id'])
            ->where('status', '=', $params['status'])
            ->where('install_method', '=', $params['install_method'])
            ->orderBy('product_id');
    }
}

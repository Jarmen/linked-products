<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Linked Products
    |--------------------------------------------------------------------------
    |
    | base_path - for routing
    | ruleset - basic rules to implement linked products
    | option_id - product characteristic slug
    | linked - array of products count => number of links matches
    */

    'parse_path' => '/linked_products/parse',
    'ruleset' => [
        'max_products_count' => 17,
        'max_links_count' => 8,
        'option_id' => 'sposob_ustanovki',
        'linked' => [
            1 => 0,
            2 => 1,
            3 => 1,
            4 => 1,
            5 => 2,
            6 => 2,
            7 => 3,
            8 => 3,
            9 => 4,
            10 => 4,
            11 => 5,
            12 => 5,
            13 => 6,
            14 => 6,
            15 => 7,
            16 => 7,
            17 => 8,
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | Out Of Prod Linked Products
    |--------------------------------------------------------------------------
    |
    | out_of_prod_parse_path - for routing
    */

    'out_of_prod_parse_path' => '/out_of_prod_linked_products/parse'
];
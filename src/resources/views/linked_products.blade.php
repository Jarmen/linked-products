{{--
$linked_products - набор перелинкованных товаров
$product->name - название товара
$product->slug - ссылка на страницу товара
$product->cost - цена на товар, если не снят с продажи
$product->image - превью изображение товара
$product->status - статус товара в зависимости от остатков
--}}
<section class="product-item-section">
	<div class="row">
		<div class="product-item-section-title">Похожие модели</div>
		<div class="linked-products-slider">

			@foreach($linked_products as $product)

				<div class="product-card">
					<div class="prod-cart-wrap">
						<div class="product-card-wrassetser">
							<a href="{{ $product->slug }}" title="">
								<img class="product-card-image" src="{{ env('APP_IMG_STORAGE') . '/' . $product->image }}" alt="" title="">
							</a>
							<!-- Image-link-->
							<div class="product-card-title">
								<a href="{{ $product->slug }}"> {{ $product->name }}</a>
							</div>
							<div class="product-price">
								@if (! empty($product_statuses[$product->status]) && $product_statuses[$product->status] == 'Снят с производства')
									Нет в продаже
								@else
									@if (! empty($product->cost))
										{{ number_format( $product->cost, 0, ',', ' ' ) }} грн
									@endif
								@endif
							</div>
							@if (! empty($product_statuses[$product->status]))
								<div class="product-status pre-order">{{ $product_statuses[$product->status] }}</div>
							@endif
							<dl class="product-card-characteristics">
								<dt>Общий объем, л:</dt>
								<dd>{{ $product->volume }}</dd>
								<dt>Габариты (ШхВхГ), см:</dt>
								<dd>{{ $product->width }}x{{ $product->height }}x{{ $product->length }}</dd>
							</dl>
							<div class="comparison-reviews-block">
								<!-- <a class="comparison-link" href="#">
                                                            <i class="icon-sravn"></i>В сравнении</a> -->
								<a class="reviews-link" href="{{ url($product->slug) }}#goods-menu5">
									<i class="icon-otz"></i>Отзывы</a>
							</div>
						</div>
					</div>
				</div>

			@endforeach

		</div>
	</div>

</section>
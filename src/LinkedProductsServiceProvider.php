<?php

namespace lenal\linkedproducts;

use Illuminate\Support\ServiceProvider;

class LinkedProductsServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->app->bind('linkedproducts', 'lenal\linkedproducts\Helpers\LinkedProducts');
        $this->app->bind('linkedlist', 'lenal\linkedproducts\Helpers\LinkedList');
    }

    public function boot()
    {
        $this->loadViewsFrom(realpath(dirname(__FILE__)) . '/resources/views', 'linkedproducts');

        require realpath(dirname(__FILE__)) . '/routes.php';
        //$this->loadRoutesFrom(realpath(dirname(__FILE__)) . '/routes.php');

        $this->publishes([
            realpath(dirname(__FILE__) . '/config/linkedproducts.php') => config_path('linkedproducts.php')
        ], 'config');

        $this->publishes([
            base_path('vendor/lenal/linkedproducts/') => base_path('packages/lenal/linkedproducts')
        ]);
    }
}
<?php

Route::get(config('linkedproducts.parse_path'), '\lenal\linkedproducts\Controllers\LinkedProductsParserController@parse');
Route::get(config('linkedproducts.out_of_prod_parse_path'), '\lenal\linkedproducts\Controllers\LinkedOutOfProdParserController@parse');
